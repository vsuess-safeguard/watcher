package storage

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"io"
)

type Storage struct {
	bucket string
	svc    *s3manager.Uploader
}

func New(bucket string, sess *session.Session) *Storage {
	return &Storage{
		bucket: bucket,
		svc:    s3manager.NewUploader(sess),
	}
}

func (s *Storage) Upload(name string, f io.Reader, deviceID string) (*string, error) {
	o, err := s.svc.Upload(&s3manager.UploadInput{
		Bucket:  aws.String(s.bucket),
		Key:     aws.String(name),
		Body:    f,
		Tagging: aws.String("device_id=" + deviceID),
	})
	if err != nil {
		return nil, err
	}

	return &o.Location, nil
}
