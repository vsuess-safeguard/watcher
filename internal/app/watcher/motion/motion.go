package motion

import (
	"time"
)

type Data struct {
	Msg    string
	Issued time.Time
}

// Run watches on the provided pin and starts an endless loop watching for a rpio.RiseEdge detection
// If an edge is detected, an event will be sent to the provided channel.
func Run(pin uint8, ch chan Data) {
	p := rpio.Pin(pin)
	if err := rpio.Open(); err != nil {
		panic(err)
	}
	defer rpio.Close()

	p.Input()
	p.Detect(rpio.RiseEdge)

	for {
		if p.EdgeDetected() {
			ch <- Data{Issued: time.Now(), Msg: "motion detected"}
		}
	}
}
