package watcher

import (
	"codeltin.io/safeguard/daemon/internal/app/daemon/queue"
	"codeltin.io/safeguard/daemon/internal/pkg/safeguardapi"
	"codeltin.io/safeguard/watcher/internal/app/safeguard/cmd"
	"codeltin.io/safeguard/watcher/internal/app/safeguard/motion"
	"codeltin.io/safeguard/watcher/internal/app/safeguard/storage"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"log"
	"os"
)

type Safeguard struct {
	sensorPIN    uint8
	storage      *storage.Storage
	device       string
	safeguardAPI safeguardapi.API
	polling      *queue.Queue

	isArmed bool
}

func NewSafeguard(sensorPIN uint8) *Safeguard {
	conf := aws.NewConfig()
	conf.WithRegion(os.Getenv("AWS_REGION"))
	conf.WithCredentials(credentials.NewStaticCredentials(os.Getenv("AWS_ACCESS_ID"), os.Getenv("AWS_ACCESS_SECRET"), ""))

	sess, err := session.NewSession(conf)
	if err != nil {
		log.Fatalf("session failed: %v", err)
	}

	return &Safeguard{
		sensorPIN: sensorPIN,
		device:    "raspberry01", // todo
		storage:   storage.New(os.Getenv("CAPTURES_BUCKET_NAME"), sess),
		polling:   queue.New(sess, nil),
	}
}

func (s *Safeguard) Run() error {
	var (
		motions  = make(chan motion.Data)
		messages = make(chan *sqs.Message)
	)

	if err := s.setup(); err != nil {
		return err
	}

	go s.polling.Poll(messages)

	go motion.Run(s.sensorPIN, motions)

	for {
		log.Println("[INFO] waiting for movement...")

		for m := range messages {
			s.polling.ReadFromMessage(m)
		}

		select {
		case m := <-motions:
			if !s.isArmed {
				log.Printf("[INFO] detected motion but device is disarmed")
				continue
			} else {
				log.Printf("%s at %s\n", m.Msg, m.Issued.String())
			}

			vid := cmd.NewRaspivid()
			if err := cmd.Exec(vid); err != nil {
				log.Printf("[ERROR] failed to capture video, %v\n", err)
				continue
			}

			f, err := os.Open(vid.FullPath())
			if err != nil {
				log.Printf("[ERROR] failed to open file, %v\n", err)
				continue
			}

			location, err := s.storage.Upload(vid.Filename(), f, s.device)
			if err != nil {
				log.Printf("[ERROR] failed to upload file, %v\n", err)
				continue
			}

			log.Printf("[INFO] video was uploaded to s3 bucket as %q\n", *location)
		}
	}
}

func (s *Safeguard) setup() error {
	log.Printf("[INFO] retrieving device information...")
	device, err := s.safeguardAPI.GetDevice(s.device)
	if err != nil {
		log.Printf("[ERROR] failed to fetch device information, err: %v", err)
		return err
	}
	s.isArmed = device.IsArmed
	log.Printf("[INFO] successfully fetched device information")

	return nil
}
