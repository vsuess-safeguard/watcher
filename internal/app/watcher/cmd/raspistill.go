package cmd

import (
	"os/exec"
	"strconv"
	"time"
)

const pictureCmd = "raspistill"

type Raspistill struct {
	Height, Width int // resolution
	Filename      string
	Output        string // directory + filename
}

func NewRaspistill() *Raspistill {
	// set defaults
	fName := time.Now().Format("2006-01-02_1504.jpeg")
	return &Raspistill{
		Height:   1080,
		Width:    1920,
		Filename: fName,
		Output:   "/home/pi/results/" + fName,
	}
}

func (r *Raspistill) Cmd() *exec.Cmd {
	return exec.Command(
		pictureCmd,
		"-w", strconv.Itoa(r.Width),
		"-h", strconv.Itoa(r.Height),
		"-o", r.Output,
	)
}
