package cmd

import (
	"fmt"
	"os/exec"
	"strings"
	"time"
)

const (
	videoCmd  = "raspivid"
	outputExt = ".mp4"
)

type Raspivid struct {
	_ struct{}

	Width, Height int // resolution, default is 1280x720
	Length        int // seconds, default is 10
	filename      string
	Output        string // output path, default is /home/pi/results/
}

// WithResolution sets the video resolution parameter.
// If argument is nil, the value is not touched.
func (r *Raspivid) WithResolution(h, w *int) *Raspivid {
	if h != nil {
		r.Height = *h
	}

	if w != nil {
		r.Width = *w
	}

	return r
}

// WithOutput sets the output path.
func (r *Raspivid) WithOutput(s string) *Raspivid {
	s = strings.TrimRight("/", s)

	r.Output = s + "/" + r.filename

	return r
}

// Filename returns the filename without extension (current time in format: 2006-01-02-1504.mp4).
func (r *Raspivid) Filename() string {
	return r.filename
}

// FullPath returns the combined Output and filename attribute.
func (r *Raspivid) FullPath() string {
	return r.Output + r.filename
}

func NewRaspivid() *Raspivid {
	// set defaults
	return &Raspivid{
		Height:   720,
		Width:    1280,
		Length:   10,
		filename: time.Now().Format("2006-01-02-1504") + outputExt,
		Output:   "/home/pi/results/",
	}
}

func (r *Raspivid) Cmd() *exec.Cmd {
	cmd := fmt.Sprintf("%s -w %d -h %d -o - -t %d | %s -i - -y -vcodec copy %s",
		videoCmd,
		r.Width,
		r.Height,
		r.Length*1000,
		"/usr/bin/ffmpeg",
		r.Output+r.filename,
	)

	return exec.Command("bash", "-c", cmd)
}
