package cmd

import (
	"fmt"
	"log"
	"os"
	"os/exec"
)

type Command interface {
	Cmd() *exec.Cmd
}

func Exec(c Command) error {
	fmt.Println(c.Cmd().String())
	c.Cmd().Stdin = os.Stdin
	c.Cmd().Stderr = os.Stderr

	return c.Cmd().Run()
	if err := c.Cmd().Start(); err != nil {
		log.Println("start")
		return err
	}

	if err := c.Cmd().Wait(); err != nil {
		log.Printf("wait")
		return err
	}

	return nil
}

func shortArg(n string, x interface{}) string {
	return fmt.Sprintf("-%s %v", n, x)
}

func arg(n string, x interface{}) string {
	return fmt.Sprintf("--%s=%v", n, x)
}
